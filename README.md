# Lexical Analyzer

A project I wrote in Lex for my Compiler Design class ([18CS61](https://vtu.ac.in/pdf/2014syll/cs.pdf)).

## Intro
A _compiler_ translates program code written in one language to another language without
changing the meaning of the program. A compiler is also expected to make target code efficient and
optimized in terms of time and space.
Compiler design covers basic translation mechanism and error detection & recovery. It
includes lexical, syntax, and semantic analysis as front end, and code generation and optimization as
back-end.

_Lexical analysis_ is the first phase of a compiler. It takes modified source code from language
preprocessors that are written in the form of sentences. The lexical analyzer breaks these syntaxes
into a series of tokens, by removing any whitespace or comments in the source code.


## Objective
By studying and making a lexical analyzer, we seek to learn about the internal workings of a
compiler. We want to see how a lexical analyzer collects also information about tokens into their
associated attributes – the tokens influence parsing decisions and the attributes influence the
translation of tokens. We also want to demonstrate how a lexical analyzer identifies errors with the
help of an automation machine and the grammar of the given language on which it is based like C,
C++, and gives row number and column number of the error. We will also create a working example
of a lexical analyzer that recognizes C programs and test it with a simple input C program file.


## Running
```
[owais@apollo] $ lex c_analyzer.l
[owais@apollo] $ gcc lex.yy.c -ly -ll -o analyze
[owais@apollo] $ ./analyze test.c
#include <stdio.h> is a PREPROCESSOR DIRECTIVE
FUNCTION
main ()
BLOCK BEGINS
int is a KEYWORD
a is an IDENTIFIER
b is an IDENTIFIER
BLOCK ENDS
Total number of comments are: 1
```

## References
- Introduction to Lexical Analysis – http://user.it.uu.se/~kostis/Teaching/KT1-11/Slides/lecture02.pdf
- Write Text Parsers with Yacc and Lex – https://developer.ibm.com/technologies/systems/tutorials/au-lexyacc/
- Compilers: Principles, Techniques and Tools – _Alfred V Aho, Monica S. Lam, Ravi Sethi, Jeffrey D Ullman_, **ISBN**: 9780201000221

## License
[This is free and unencumbered software released into the public domain. Click here for license.](LICENSE)
